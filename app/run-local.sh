#!/bin/bash

set -ueo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

meteor npm install

if [ -f .env.local ]
then
    set -o allexport
    source .env.local
    set +o allexport
fi

MONGO_URL=mongodb://localhost/norless \
MONGO_OPLOG_URL=mongodb://localhost/local \
meteor run \
--port 1355 \
--exclude-archs "web.browser.legacy" \

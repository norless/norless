import map from "lodash/map";
import PropTypes from "prop-types";
import React from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { Edit } from "styled-icons/feather/Edit.cjs.js";

import { HeaderContainer, headerContainerHeight } from "./Containers.jsx";
import {
    displaySlideNext,
    displaySlidePrev,
    displaySlideSet,
} from "./OutputContainer.js";

const HeaderTitle = styled.div`
    color: white;
    font-size: 20px;
    line-height: ${headerContainerHeight}px;
    padding-left: ${16 + 2}px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
`;

const HeaderButton = styled.div`
    position: absolute;
    background: rgba(35, 124, 215, 0.8); /* #237cd7 */
    color: #ffffff;
    top: 0;
    right: 0;
    line-height: ${headerContainerHeight}px;
    text-align: center;
    padding: 0 16px 0 8px;
`;

const Slide = styled.li`
    cursor: pointer;
    white-space: pre-wrap;
    background: #ffffff;
    border: 2px solid transparent;
    border-bottom: 1px dotted #e3e3e3;
    padding: 16px 16px 16px 16px; /* padding substracted below */
    font-size: 14px;
    line-height: 18px;
    ${(props) =>
        props.selected
            ? `
                padding-bottom: 15px;
                border: 2px solid #d7237c;
            `
            : ""}

    .chord {
        display: none;
    }

    .charpad {
        display: none;
    }
`;

// TODO display without danger
const EntrySlide = ({ text, selected, onClick }) => (
    <Slide
        selected={selected}
        onClick={onClick}
        dangerouslySetInnerHTML={{
            __html: text,
        }}
    />
);

EntrySlide.propTypes = {
    text: PropTypes.string,
    selected: PropTypes.bool,
    onClick: PropTypes.func,
};

const SongEditButton = ({ songId }) => {
    const history = useHistory();

    return (
        <HeaderButton onClick={() => history.push(`/song/${songId}`)}>
            <Edit size={24} />
        </HeaderButton>
    );
};

SongEditButton.propTypes = {
    songId: PropTypes.string,
};

const HandleHotkeys = () => {
    // TODO defaults can be configured
    // https://www.npmjs.com/package/react-hotkeys#configuration
    const options = {
        enableOnTags: ["INPUT", "TEXTAREA", "SELECT"],
    };

    useHotkeys(
        "pageUp",
        (e) => {
            e.preventDefault();
            displaySlidePrev();
        },
        options,
    );

    useHotkeys(
        "pageDown",
        (e) => {
            e.preventDefault();
            displaySlideNext();
        },
        options,
    );

    return null;
};

const EntrySlides = ({ title, slides, canEdit = false, entry }) => (
    <>
        <HeaderContainer>
            <HeaderTitle>{title}</HeaderTitle>
            {canEdit && <SongEditButton songId={entry.identifier} />}
        </HeaderContainer>

        <ul className="slides">
            {map(slides, (slide, index) => (
                <EntrySlide
                    key={index}
                    {...slide}
                    onClick={() => {
                        displaySlideSet(slide);
                    }}
                />
            ))}
        </ul>

        <HandleHotkeys />
    </>
);

EntrySlides.propTypes = {
    title: PropTypes.string,
    slides: PropTypes.array,
    canEdit: PropTypes.bool,
    entry: PropTypes.object,
};

export default EntrySlides;

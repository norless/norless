import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import PlaylistsRepository from "/imports/models/Playlists.js";

import ListOfPlaylists from "./ListOfPlaylists.jsx";

const ListOfPlaylistsContainer = withTracker(() => {
    const subscription = Meteor.subscribe("list_of_playlists");
    const loading = !subscription.ready();
    const playlists = PlaylistsRepository.find(
        {},
        {
            sort: {
                position: -1,
            },
        },
    ).fetch();

    return {
        loading,
        playlists,
    };
})(ListOfPlaylists);

export default ListOfPlaylistsContainer;

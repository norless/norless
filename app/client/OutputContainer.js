import { withTracker } from "meteor/react-meteor-data";

import Output from "./Output.jsx";

export const displaySlide = new ReactiveVar({});

export const displaySlideSet = (slide) =>
    displaySlide.set({
        ...slide,
        active: true,
    });
export const displaySlideClear = () => displaySlide.set({});

const displaySlideAdvance = (direction) => {
    console.log("TODO from live_change_slide", direction);
};

export const displaySlidePrev = () => displaySlideAdvance(-1);
export const displaySlideNext = () => displaySlideAdvance(1);

const OutputContainer = withTracker(() => ({
    title: "Title",
    key_signature: "Do",
    time_signature: "4/4",
    slide_index: 2,
    slide_count: 7,
    next_line: "Next line",

    ...displaySlide.get(),
}))(Output);

export default OutputContainer;

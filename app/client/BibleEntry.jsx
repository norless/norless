import PropTypes from "prop-types";
import React from "react";
import { BookOpen } from "styled-icons/feather/BookOpen.cjs.js";

import { IconHolder, Title } from "./StyledEntry.jsx";

const BibleEntry = ({ _id }) => {
    const title = _id;

    return (
        <React.Fragment>
            <IconHolder size={56}>
                <BookOpen size={24} />
            </IconHolder>

            <Title>{title}</Title>
        </React.Fragment>
    );
};

export default BibleEntry;

BibleEntry.propTypes = {
    _id: PropTypes.string,
};

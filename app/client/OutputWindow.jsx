import React from "react";
import PropTypes from "prop-types";

import OutputContainer from "./OutputContainer.js";
import { StyledWindowPortal } from "/imports/components/StyledWindowPortal.jsx";

const OutputWindow = ({ opened, setOpened }) => {
    if (!opened) {
        return null;
    }

    return (
        <StyledWindowPortal
            title="Norless Output"
            onClose={() => setOpened(false)}
        >
            <OutputContainer />
        </StyledWindowPortal>
    );
};

OutputWindow.propTypes = {
    opened: PropTypes.bool,
    setOpened: PropTypes.func,
};

export default OutputWindow;

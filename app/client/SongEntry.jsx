import SongsRepository from "/imports/models/Songs.js";
import get from "lodash/get";
import map from "lodash/map";
import PropTypes from "prop-types";
import React from "react";
import { Music } from "styled-icons/feather/Music.cjs.js";

import { IconHolder, Metadata, Tag, Title } from "./StyledEntry.jsx";

const SongEntry = ({ identifier, ...object }) => {
    const song = identifier
        ? SongsRepository.findOne({ _id: identifier })
        : object;

    return (
        <React.Fragment>
            <IconHolder size={75}>
                <Music size={24} />
            </IconHolder>

            <Title>{get(song, "title") || <em>(no title)</em>}</Title>
            <Metadata>
                <Tag>{get(song, "key_signature")}</Tag>
                <Tag>{get(song, "time_signature")}</Tag>
                {map(get(song, "tags"), (tag) => (
                    <Tag key={tag}>{tag}</Tag>
                ))}
            </Metadata>
        </React.Fragment>
    );
};

export default SongEntry;

SongEntry.propTypes = {
    identifier: PropTypes.string,
    object: PropTypes.object,
};

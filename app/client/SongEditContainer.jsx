import SongsRepository from "/imports/models/Songs.js";
import { withTracker } from "meteor/react-meteor-data";
import React from "react";
import { useParams } from "react-router-dom";

import SongEdit from "./SongEdit.jsx";

const SongEditTracker = withTracker(({ songId }) => {
    Meteor.subscribe("song_tags");
    const subscription = Meteor.subscribe("song", { songId });
    const loading = !subscription.ready();

    const song = SongsRepository.findOne({ _id: songId });

    return {
        loading,
        song,
    };
})(SongEdit);

const SongEditContainer = () => {
    const { songId } = useParams();

    return <SongEditTracker songId={songId} />;
};

export default SongEditContainer;

import React from "react";
import get from "lodash/get";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { Link, useParams } from "react-router-dom";

import Loader from "./Loader.jsx";
import User from "/imports/ui/User.jsx";

const Container = styled.div`
    overflow-x: hidden;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
    height: 150px;
    width: 100%;
`;

const Playlist = styled(Link)`
    display: block;
    padding: 5px 5px 5px 15px;
    border: 1px solid transparent;
    cursor: pointer;
    margin-top: 0px;
    line-height: 16px !important;

    &:hover {
        border-color: #c5c5c5;
        background: #ececec;
    }

    ${props =>
        props.selected &&
        css`
            border-color: #727272;
            background: #dadada;
            z-index: 901;
            position: relative;
        `}
`;

const Title = styled.div`
    color: #3d3d3d;
    font-size: 1.2em;
`;

const Creator = styled.div`
    color: #838383;
    font-size: 14px;
`;

const ListOfPlaylists = ({ loading, playlists }) => {
    if (loading) {
        return <Loader />;
    }

    const playlistId = useParams().playlistId || get(playlists, "0._id");

    return (
        <Container>
            {playlists.map((playlist, playlistIndex) => (
                <Playlist
                    key={playlist._id}
                    to={playlistIndex === 0 ? "/" : `/playlist/${playlist._id}`}
                    onClick={() => window.toggleBurgerMenu(false)}
                    selected={playlist._id === playlistId}
                >
                    <Title>{playlist.title}</Title>
                    <Creator>
                        <User.IdToIconName id={playlist.creator} />
                    </Creator>
                </Playlist>
            ))}
        </Container>
    );
};

ListOfPlaylists.propTypes = {
    loading: PropTypes.bool,
    playlists: PropTypes.array,
};

export default ListOfPlaylists;

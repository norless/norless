import { withTracker } from "meteor/react-meteor-data";

import OutputWindow from "./OutputWindow.jsx";
import outputWindowState from "./outputWindowState.js";

const OutputWindowContainer = withTracker(() => ({
    opened: outputWindowState.get(),
    setOpened: state => outputWindowState.set(state),
}))(OutputWindow);

export default OutputWindowContainer;

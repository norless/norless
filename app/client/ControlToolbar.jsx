import { useTracker } from "meteor/react-meteor-data";
import React from "react";
import styled from "styled-components";
import { ChevronLeft } from "styled-icons/feather/ChevronLeft.cjs.js";
import { ChevronRight } from "styled-icons/feather/ChevronRight.cjs.js";
import { Square } from "styled-icons/feather/Square.cjs.js";
import { XSquare } from "styled-icons/feather/XSquare.cjs.js";

import {
    displaySlide,
    displaySlideClear,
    displaySlideNext,
    displaySlidePrev,
} from "./OutputContainer";

const BottomToolbar = styled.div`
    display: flex;
    flex-direction: row;
    padding: 10px;
    background: rgba(255, 255, 255, 0.87);
    color: rgba(0, 0, 0, 0.54);
    transition: bottom 0.7s;
    transition-timing-function: cubic-bezier(0.07, 0.895, 0.11, 1);
    /* http://matthewlein.com/ceaser/ custom */

    ${(props) =>
        props.isClosed
            ? `
            bottom: -56px;
            `
            : ""}
`;

const ToolbarIconContainer = styled.div`
    flex: 1;
    cursor: pointer;
    align-self: center;
    padding: 10px;
`;

const ControlToolbar = () => {
    // TODO if not admin return

    const { isClosed } = useTracker(() => ({
        isClosed: !displaySlide.get().active,
    }));

    // TODO detect
    const isMobileDevice = true;

    return (
        <BottomToolbar isClosed={false && isClosed}>
            {isMobileDevice && (
                <ToolbarIconContainer>
                    <Square size={24} />
                </ToolbarIconContainer>
            )}

            <ToolbarIconContainer onClick={displaySlideClear}>
                <XSquare size={24} />
            </ToolbarIconContainer>

            <ToolbarIconContainer onClick={displaySlidePrev}>
                <ChevronLeft size={24} />
            </ToolbarIconContainer>

            <ToolbarIconContainer onClick={displaySlideNext}>
                <ChevronRight size={24} />
            </ToolbarIconContainer>
        </BottomToolbar>
    );
};

export default ControlToolbar;

import get from "lodash/get";
import PropTypes from "prop-types";
import React from "react";

import BibleEntry from "./BibleEntry.jsx";
import DividerEntry from "./DividerEntry.jsx";
import SongEntry from "./SongEntry.jsx";
import UnknownEntry from "./UnknownEntry.jsx";

const types = {
    song: { size: 75, component: SongEntry },
    bible: { size: 56, component: BibleEntry },
    divider: { size: 56, component: DividerEntry },
    default: { size: 56, component: UnknownEntry },
};

export const getEntryItemSize = (e) =>
    (types[get(e, "type")] || types.default).size;

const EntryItem = (props) => {
    const Component = (types[props.type] || types.default).component;

    return <Component {...props} />;
};

export default EntryItem;

EntryItem.propTypes = {
    type: PropTypes.string,
    identifier: PropTypes.string,
};

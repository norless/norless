import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";

import { FormGroup, InputGroup, MenuItem, TextArea } from "@blueprintjs/core";
import { MultiSelect } from "@blueprintjs/select";
import SongsRepository from "/imports/models/Songs.js";
import { Field, Form, Formik, useFormikContext } from "formik";
import countBy from "lodash/countBy";
import flatMap from "lodash/flatMap";
import includes from "lodash/includes";
import map from "lodash/map";
import orderBy from "lodash/orderBy";
import without from "lodash/without";
import PropTypes from "prop-types";
import React, { useCallback } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useHistory } from "react-router-dom";
import { Grid } from "semantic-ui-react";
import styled from "styled-components";

import Loader from "./Loader.jsx";

const SongEditWrapper = styled.div`
    background: #b7dde4;
    position: relative;
`;

const TextAreaMono = styled(TextArea)`
    font-family: monospace;
`;

const getTagItems = () =>
    orderBy(
        map(
            countBy(flatMap(SongsRepository.find().fetch(), "tags")),
            (count, title) => ({
                title,
                count,
            }),
        ),
        "count",
        "desc",
    );

const renderCreateFilmOption = (query, active, handleClick) => (
    <MenuItem
        icon="add"
        text={`Create "${query}"`}
        active={active}
        onClick={handleClick}
        shouldDismissPopover={false}
    />
);

const HandleHotkeys = ({ cancel }) => {
    const { submitForm } = useFormikContext();

    // TODO defaults can be configured
    // https://www.npmjs.com/package/react-hotkeys#configuration
    const options = {
        enableOnTags: ["INPUT", "TEXTAREA", "SELECT"],
    };

    useHotkeys(
        "ctrl+s, command+s",
        (e) => {
            e.preventDefault();
            submitForm();
        },
        options,
    );

    useHotkeys("esc", cancel, options);

    return null;
};

const SongEdit = ({ loading, song }) => {
    const history = useHistory();

    const goHome = useCallback(() => {
        history.push("/");
    });

    if (loading) {
        return <Loader />;
    }

    if (!song) {
        return <div className="alert alert-info">Select a song to edit</div>;
    }

    return (
        <SongEditWrapper>
            <Formik
                initialValues={song}
                onSubmit={(values, { setSubmitting }) => {
                    Meteor.call(
                        "songUpdate",
                        {
                            _id: song._id,
                            values,
                        },
                        () => {
                            setSubmitting(false);
                        },
                    );
                }}
            >
                {({ values, isSubmitting, setFieldValue }) => (
                    <Form>
                        <HandleHotkeys cancel={goHome} />
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <button onClick={goHome}>Close</button>
                                    <button
                                        type="submit"
                                        disabled={isSubmitting}
                                    >
                                        Save
                                    </button>
                                </Grid.Column>
                                <Grid.Column>
                                    <FormGroup label="Song Title">
                                        <Field
                                            as={InputGroup}
                                            name="title"
                                            placeholder="Song Title"
                                        />
                                    </FormGroup>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <FormGroup label="Key Signature">
                                        <Field
                                            as={InputGroup}
                                            name="key_signature"
                                            placeholder="Key Signature"
                                        />
                                    </FormGroup>
                                </Grid.Column>
                                <Grid.Column>
                                    <FormGroup label="Time Signature">
                                        <Field
                                            as={InputGroup}
                                            name="time_signature"
                                            placeholder="Time Signature"
                                        />
                                    </FormGroup>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row columns={1}>
                                <Grid.Column>
                                    {/* https://github.com/palantir/blueprint/blob/develop/packages/docs-app/src/examples/select-examples/multiSelectExample.tsx */}
                                    <MultiSelect
                                        items={getTagItems()}
                                        itemsEqual={(a, b) =>
                                            a.title === b.title
                                        }
                                        selectedItems={map(
                                            values.tags,
                                            (tag) => ({
                                                title: tag,
                                            }),
                                        )}
                                        itemRenderer={(
                                            tagObject,
                                            { modifiers, handleClick },
                                        ) => {
                                            if (!modifiers.matchesPredicate) {
                                                return null;
                                            }
                                            return (
                                                <MenuItem
                                                    active={modifiers.active}
                                                    icon={
                                                        includes(
                                                            values.tags,
                                                            tagObject.title,
                                                        )
                                                            ? "tick"
                                                            : "blank"
                                                    }
                                                    key={tagObject.title}
                                                    label={tagObject.count}
                                                    onClick={handleClick}
                                                    text={tagObject.title}
                                                    shouldDismissPopover={false}
                                                />
                                            );
                                        }}
                                        onItemSelect={(tagObject) => {
                                            if (
                                                includes(
                                                    values.tags,
                                                    tagObject.title,
                                                )
                                            ) {
                                                // remove tag
                                                setFieldValue(
                                                    "tags",
                                                    without(
                                                        values.tags,
                                                        tagObject.title,
                                                    ),
                                                );
                                            } else {
                                                // add tag
                                                setFieldValue("tags", [
                                                    ...values.tags,
                                                    tagObject.title,
                                                ]);
                                            }
                                        }}
                                        onRemove={(tagObject) => {
                                            // remove tag
                                            setFieldValue(
                                                "tags",
                                                without(
                                                    values.tags,
                                                    tagObject.title,
                                                ),
                                            );
                                        }}
                                        createNewItemFromQuery={(tag) => ({
                                            title: tag,
                                            createNew: true,
                                        })}
                                        createNewItemRenderer={
                                            renderCreateFilmOption
                                        }
                                        tagRenderer={(tagObject) =>
                                            tagObject.title
                                        }
                                    />
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row columns={1}>
                                <Grid.Column>
                                    <Field
                                        as={TextAreaMono}
                                        growVertically={true}
                                        large={true}
                                        fill={true}
                                        name="text"
                                        placeholder="Lyrics and Chords"
                                    />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </SongEditWrapper>
    );
};

SongEdit.propTypes = {
    loading: PropTypes.bool,
    song: PropTypes.object,
};

export default SongEdit;

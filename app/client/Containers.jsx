import styled from "styled-components";

const padding = 12;
const searchInputHeight = 38;
const headerContainerHeight = padding + searchInputHeight + padding;

const HeaderContainer = styled.div`
    background: #237cd7;
    height: ${headerContainerHeight}px;
    position: relative;
`;

export { padding, headerContainerHeight, HeaderContainer };

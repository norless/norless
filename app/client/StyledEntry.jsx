import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";

export const IconHolder = styled.div`
    position: absolute;
    left: 16px;
    line-height: ${(props) => props.size}px;
`;

export const Title = styled.div`
    position: absolute;
    left: 72px;
    right: 16px;
    top: 16px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    line-height: 24px;
    font-size: 16px;
    opacity: 0.87;
`;

export const DividerTitle = styled(Title)`
    font-size: 14px;
    color: #237cd7;
    text-transform: uppercase;
    letter-spacing: 1px;
    font-weight: 500;
`;

export const Metadata = styled.div`
    position: absolute;
    left: 72px;
    right: 16px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    line-height: 22px;
    font-size: 14px;
    font-weight: 300;
    opacity: 0.54;
    bottom: 16px;
`;

const TagElement = styled.span`
    font-size: 80%;
    margin-right: 6px;
    padding: 2px 8px;
    border-radius: 2px;
    background: #eeeeee;
`;

export const Tag = ({ children }) => {
    if (!children) {
        return null;
    }

    return <TagElement>{children}</TagElement>;
};

Tag.propTypes = {
    children: PropTypes.node,
};

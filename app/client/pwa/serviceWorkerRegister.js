if ("serviceWorker" in navigator) {
    Meteor.startup(() => {
        navigator.serviceWorker
            .register("/pwaServiceWorker.js")
            .then(() => {
                // successful
            })
            .catch(error =>
                console.error("ServiceWorker registration failed", error),
            );
    });
}

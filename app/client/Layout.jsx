import { BurgerMenuContent } from "/imports/components/BurgerMenu.jsx";
import useWindowSize from "/imports/hocs/useWindowSize.js";
import React from "react";
import styled from "styled-components";

import ControlToolbar from "./ControlToolbar.jsx";
import EntrySlidesContainer from "./EntrySlidesContainer.jsx";
import NotFound from "./NotFound.jsx";
import PlaylistContainer from "./PlaylistContainer.jsx";
import PreviewContainer from "./PreviewContainer.js";
import SearchContainer from "./SearchContainer.jsx";
import SongEdit from "./SongEdit.jsx";

const ColumnWrapper = styled.div`
    display: flex;
`;

const Column = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    overflow: hidden;
`;

const Container = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;

    display: flex;
    flex-direction: column;
`;

const Section = styled.div`
    flex-grow: 1;

    display: flex;
    flex-direction: column;

    /* for Firefox */
    min-height: 0;
`;

const Spacer = styled.div`
    margin-left: auto;
`;

const Footer = styled.div`
    display: flex;
    border-top: 1px solid #dddddd;
`;

const Scrollable = styled.div`
    background: white;
    flex-grow: 1;

    overflow: auto;

    /* for Firefox */
    min-height: 0;
`;

const LayoutLandscape = () => (
    <Container>
        <Section>
            <Scrollable>
                <ColumnWrapper>
                    <Column>
                        <SearchContainer />
                        <PlaylistContainer />
                    </Column>
                    <Column>
                        <EntrySlidesContainer />
                    </Column>
                    <Column>
                        <SongEdit />
                        <NotFound />
                    </Column>
                </ColumnWrapper>
            </Scrollable>
            <Footer>
                <Spacer />
                <PreviewContainer />
                <ControlToolbar />
                <Spacer />
            </Footer>
        </Section>
    </Container>
);

const LayoutPortrait = () => (
    <>
        <SearchContainer />
        <PlaylistContainer />
    </>
);

const Layout = () => {
    const size = useWindowSize();
    const isLandscape = size.width > size.height;
    const SelectedLayout = isLandscape ? LayoutLandscape : LayoutPortrait;

    return (
        <>
            <BurgerMenuContent />
            <SelectedLayout />
        </>
    );
};

export default Layout;

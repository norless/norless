import React from "react";
import map from "lodash/map";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Monitor } from "styled-icons/feather/Monitor.cjs.js";
import { Smartphone } from "styled-icons/feather/Smartphone.cjs.js";

import { IconName } from "/imports/ui/User.jsx";

const Container = styled.div`
    overflow-x: hidden;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
    height: 150px;
    width: 100%;
`;

const User = styled(Link)`
    display: block;
    padding: 5px 5px 5px 15px;
    border: 1px solid transparent;
    cursor: pointer;
    margin-top: 0px;
    line-height: 16px !important;
`;

const Right = styled.div`
    float: right;
`;

const OnlineIcon = ({ user }) => {
    if (!user.isOnline) {
        return null;
    }

    if (user.isMobile) {
        return <Smartphone size={16} />;
    }

    return <Monitor size={16} />;
};

OnlineIcon.propTypes = {
    user: PropTypes.object,
};

const ListOfUsers = ({ users }) => (
    <Container>
        {map(users, user => (
            <User key={user._id} to={`/user/${user._id}`}>
                <IconName user={user} />
                <Right>
                    <OnlineIcon user={user} />
                </Right>
            </User>
        ))}
    </Container>
);

ListOfUsers.propTypes = {
    users: PropTypes.array,
};

export default ListOfUsers;

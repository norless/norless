import defer from "lodash/defer";
import findIndex from "lodash/findIndex";
import get from "lodash/get";
import isEqual from "lodash/isEqual";
import size from "lodash/size";
import PropTypes from "prop-types";
import React from "react";
import { GlobalHotKeys } from "react-hotkeys";
import {
    SortableContainer,
    SortableElement,
    arrayMove,
} from "react-sortable-hoc";
import { Header, Icon } from "semantic-ui-react";
import styled from "styled-components";

import { setSelectedEntry } from "./EntrySlidesContainer.jsx";
import Loader from "./Loader.jsx";
import PlaylistEntry from "./PlaylistEntry.jsx";
import { focusSearch } from "./SearchContainer.jsx";

const SortableItem = SortableElement(PlaylistEntry);

const PlaylistWrapper = styled.ul`
    overflow: auto;
    height: 100%;
`;

const SortableList = SortableContainer(({ items }) => (
    <PlaylistWrapper>
        {items.map((value, index) => (
            <SortableItem
                key={`item-${index}`}
                index={index}
                value={{ ...value, index }}
            />
        ))}
    </PlaylistWrapper>
));

export default class Playlist extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            entries: get(props, "playlist.entries") || [],
            selected: null,
            selectedIndex: null,
            sortHash: null,
            waitingSortSync: false,
        };
    }

    componentDidUpdate(prevProps) {
        const entries = get(this.props, "playlist.entries") || [];

        if (!isEqual(entries, get(prevProps, "playlist.entries"))) {
            if (!isEqual(entries, this.state.entries)) {
                this.setState({ entries });
            }
        }

        if (
            get(prevProps, "playlist.sorting.sortHash") &&
            !get(this.props, "playlist.sorting.sortHash") &&
            this.state.sortHash
        ) {
            // sorting waits for server response to prevent playlist flicker after sorting
            this.setState({
                sortHash: null,
                waitingSortSync: false,
            });
        }
    }

    onSortStart = ({ index }) => {
        const sortHash = Random.id();
        this.setState({ sortHash });

        const entry = this.state.entries[index];

        this.props.onSort({
            entry: {
                type: entry.type,
                identifier: entry.identifier,
            },
            active: true,
            oldIndex: index,
            newIndex: index,
            sortHash,
        });
    };

    onSortOver = ({ index, newIndex }) => {
        const entry = this.state.entries[index];

        this.props.onSort({
            entry: {
                type: entry.type,
                identifier: entry.identifier,
            },
            active: true,
            oldIndex: index,
            newIndex,
            sortHash: this.state.sortHash,
        });
    };

    onSortEnd = ({ oldIndex, newIndex }) => {
        const entries = arrayMove(this.state.entries, oldIndex, newIndex);

        // instantly update client
        this.setState({
            entries,
            waitingSortSync: true,
        });

        // send update to server
        this.props.setSortedEntries(entries);
    };

    onEntryClick = (entry) => {
        const index = findIndex(this.state.entries, {
            type: entry.type,
            identifier: entry.identifier,
        });

        this.setState({
            selected: entry,
            selectedIndex: index || null,
        });

        setSelectedEntry(entry);
    };

    selectNew = (direction) => {
        defer(() => {
            this.setState((state) => {
                const currentIndex =
                    state.selectedIndex !== null ? state.selectedIndex : -1;
                const selectedIndex = currentIndex + direction;
                const selected = state.entries[selectedIndex];

                if (!selected) {
                    return {};
                }

                setSelectedEntry(selected);

                return { selected, selectedIndex };
            });
        });
    };

    selectDown = (event) => {
        this.selectNew(+1);
        event.preventDefault();
    };

    selectUp = (event) => {
        this.selectNew(-1);
        event.preventDefault();
    };

    selectEnter = (event) => {
        // TODO
        console.log("Playlist selectEnter", event);

        event.preventDefault();
    };

    selectDelete = (event) => {
        // TODO from remove_playlist_entry
        console.log("Playlist selectDelete", event);

        event.preventDefault();
    };

    render() {
        if (this.props.loading) {
            return <Loader />;
        }

        if (!size(this.state.entries)) {
            return (
                <Header as="h2" icon textAlign="center" onClick={focusSearch}>
                    <Icon name="search" circular />
                    <Header.Content>
                        Tap on search icon to add songs.
                    </Header.Content>
                </Header>
            );
        }

        const list = (
            <SortableList
                items={this.state.entries.map((entry) => {
                    const e = { ...entry };

                    e.onClick = this.onEntryClick;

                    if (
                        e.identifier === get(this.state.selected, "identifier")
                    ) {
                        e.selected = true;
                    }

                    e.entries = this.state.entries;
                    e.sorting = this.state.waitingSortSync
                        ? null
                        : get(this.props.playlist, "sorting");
                    e.moveOnPassiveDevice =
                        this.state.sortHash !==
                        get(this.props.playlist, "sorting.sortHash");

                    return e;
                })}
                onSortStart={this.onSortStart}
                onSortOver={this.onSortOver}
                onSortEnd={this.onSortEnd}
                lockAxis="y"
                pressDelay={300}
                helperClass="currentlySortingActive"
            />
        );

        return (
            <>
                <GlobalHotKeys
                    keyMap={{
                        SELECT_DOWN: "down",
                        SELECT_UP: "up",
                        SELECT_ENTER: "enter",
                        SELECT_DELETE: ["del", "backspace"],
                    }}
                    handlers={{
                        SELECT_DOWN: this.selectDown,
                        SELECT_UP: this.selectUp,
                        SELECT_ENTER: this.selectEnter,
                        SELECT_DELETE: this.selectDelete,
                    }}
                />
                {list}
            </>
        );
    }
}

Playlist.propTypes = {
    loading: PropTypes.bool,
    playlist: PropTypes.object,
    onSort: PropTypes.func,
    setSortedEntries: PropTypes.func,
};

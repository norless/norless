import classNames from "classnames/bind";
import get from "lodash/get";
import slice from "lodash/slice";
import sumBy from "lodash/sumBy";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";

import EntryItem, { getEntryItemSize } from "./EntryItem.jsx";

const Item = styled.li`
    background: #ffffff;
    height: ${(props) => props.size}px;
    position: relative;
    cursor: pointer;

    &.currentlySorting,
    &.currentlySortingActive {
        background: #ff8855;
    }

    &.currentlySortingActive {
        box-shadow: 0 2px 25px -5px rgba(0, 0, 0, 0.3);
    }

    &.selected {
        background: #a7caef;
    }
`;

const Current = styled.div`
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    border: 2px solid #d7237c;
`;

const PlaylistEntry = ({ value }) => {
    const {
        type,
        identifier,
        selected,
        onClick,
        index,
        sorting,
        entries,
        moveOnPassiveDevice,
    } = value;

    const oldIndex = get(sorting, "oldIndex");
    const newIndex = get(sorting, "newIndex");
    const currentlySorting = oldIndex === index;

    const style = {};
    if (moveOnPassiveDevice) {
        // move elements only on other watching devices

        if (currentlySorting) {
            // move sorting element to its position
            style.zIndex = 1;

            if (newIndex < oldIndex) {
                // moved down
                style.top = -sumBy(
                    slice(entries, newIndex, oldIndex),
                    getEntryItemSize,
                );
            }

            if (newIndex > oldIndex) {
                // moved down
                style.top = sumBy(
                    slice(entries, oldIndex + 1, newIndex + 1),
                    getEntryItemSize,
                );
            }
        } else {
            // move other elements to make room for sorting element
            if (oldIndex <= index && index <= newIndex) {
                style.top = -getEntryItemSize(entries[oldIndex]);
            }

            if (oldIndex >= index && index >= newIndex) {
                style.top = getEntryItemSize(entries[oldIndex]);
            }
        }
    }

    if (!window.selectedEntry) {
        window.selectedEntry = identifier;
    }

    return (
        <Item
            size={getEntryItemSize({ type })}
            onClick={() => onClick(value)}
            className={classNames({ selected, currentlySorting })}
            style={style}
        >
            <EntryItem type={type} identifier={identifier} />

            {window.selectedEntry === identifier && <Current />}
        </Item>
    );
};

export default PlaylistEntry;

PlaylistEntry.propTypes = {
    value: PropTypes.object,
};

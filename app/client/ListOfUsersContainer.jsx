import map from "lodash/map";
import each from "lodash/each";
import sortBy from "lodash/sortBy";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import { getName } from "/imports/ui/User.jsx";
import UserConnections from "/imports/models/UserConnections.js";

import ListOfUsers from "./ListOfUsers.jsx";

const ListOfUsersContainer = withTracker(() => {
    const subscription = Meteor.subscribe("userStatus");
    const loading = !subscription.ready();

    const users = [];

    // add online connections
    each(
        UserConnections.find({
            // todo filter by location_id
        }).fetch(),
        connection => {
            const isMobile = connection.userAgent.match(
                /(iPod|iPhone|iPad|Android|Windows Phone)/,
            );

            users.push({
                ...(connection.userId
                    ? Meteor.users.findOne({ _id: connection.userId })
                    : {}),
                sort_key: connection.userId
                    ? `a_${getName(connection).toLowerCase()}`
                    : "b_",
                isOnline: true,
                isMobile,
            });
        },
    );

    // add offline users
    each(
        Meteor.users
            .find({
                // todo filter by location_id
                _id: { $nin: map(users, "_id") },
            })
            .fetch(),
        user => {
            users.push({
                ...user,
                sort_key: `c_${getName(user).toLowerCase()}`,
            });
        },
    );

    return {
        loading,
        users: sortBy(users, "sort_key"),
    };
})(ListOfUsers);

export default ListOfUsersContainer;

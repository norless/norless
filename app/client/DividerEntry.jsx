import PropTypes from "prop-types";
import React from "react";

import { DividerTitle } from "./StyledEntry.jsx";

const DividerEntry = ({ _id }) => (
    <React.Fragment>
        <DividerTitle>{_id}</DividerTitle>
    </React.Fragment>
);

export default DividerEntry;

DividerEntry.propTypes = {
    _id: PropTypes.string,
};

import { BurgerMenuOpener } from "/imports/components/BurgerMenu.jsx";
import { searchBooks } from "/imports/models/Bible.js";
import debounce from "lodash/debounce";
import size from "lodash/size";
import React from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { Search } from "semantic-ui-react";
import styled from "styled-components";

import { HeaderContainer, padding } from "./Containers.jsx";
import EntryItem, { getEntryItemSize } from "./EntryItem.jsx";
import { displaySlideClear } from "./OutputContainer.js";

const SearchQueryArea = styled.div`
    position: absolute;
    left: 56px;
    right: ${padding}px;
    top: ${padding}px;
`;

const StyledSearch = styled(Search)`
    .ui.input {
        width: 100%;
    }

    &.ui.search > .results .result {
        padding: 0;
    }
`;

export const focusSearch = () => {
    // Semantic ui react components don't expose inner ref.
    // Please use React ref for focusing when switching to other components.
    document.querySelector(".search-query-area input").focus();
};

const blurSearch = () => {
    // Semantic ui react components don't expose inner ref.
    // Please use React ref for focusing when switching to other components.
    document.querySelector(".search-query-area input").blur();
};

const addSearchResultToPlaylist = ({ result }) => {
    const { type, ...values } = result;

    if (type === "newSong") {
        // create new song and add it to the playlist
        Meteor.call(
            "songInsert",
            {
                values,
            },
            (error, { songId }) => {
                if (error) {
                    console.error("Cannot insert song", error);
                    return;
                }

                addSearchResultToPlaylist({
                    result: {
                        _id: songId,
                        ...values,
                    },
                });
            },
        );
    }

    // TODO
    console.log("addSearchResultToPlaylist", result);
};

const defaultState = {
    isLoading: false,
    results: [],
    value: "",
    focused: false,
};

const Hotkeys = ({ isSearching }) => {
    const options = {
        enableOnTags: ["INPUT", "TEXTAREA", "SELECT"],
    };

    useHotkeys("esc", isSearching ? blurSearch : displaySlideClear, options);

    useHotkeys("*", (e) => {
        if (isSearching) {
            return;
        }

        // send keys to search even when it is not focused
        if (/^[a-z0-9]$/i.test(e.key)) {
            focusSearch();
        }
    });

    return null;
};

const SearchResultItem = styled.li`
    height: ${(props) => props.size}px;
    position: relative;
    cursor: pointer;
`;

const resultRenderer = (result) => (
    <SearchResultItem size={getEntryItemSize(result)}>
        <EntryItem {...result} />
    </SearchResultItem>
);

class SearchContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = defaultState;
    }

    resetComponent = () => this.setState(defaultState);

    handleResultSelect = (_, { result }) => {
        addSearchResultToPlaylist({ result });

        this.setState({ value: "" });
    };

    handleOnFocus = () => {
        this.setState({ focused: true });
    };

    handleOnBlur = () => {
        this.setState({ focused: false });
        this.setState({ value: "" });
    };

    getSearchResults = debounce((query) => {
        const bibleResults = searchBooks(query);

        if (size(bibleResults)) {
            this.setState({
                isLoading: false,
                results: bibleResults,
            });

            return;
        }

        Meteor.call(
            "getSearchResults",
            {
                query,
            },
            (error, results) => {
                if (error) {
                    console.error("Cannot get search results", error);
                    return;
                }

                this.setState({
                    isLoading: false,
                    results: [
                        ...results,
                        {
                            type: "divider",
                            title: query,
                        },
                        {
                            type: "newSong",
                            title: query,
                        },
                    ],
                });
            },
        );
    }, 300);

    handleSearchChange = debounce((e, { value }) => {
        this.setState({ isLoading: true, value });

        this.getSearchResults(value);
    });

    render() {
        return (
            <HeaderContainer>
                <BurgerMenuOpener />

                <SearchQueryArea className="search-query-area">
                    {/*
                    maybe replace with blueprintjs suggest?
                    https://blueprintjs.com/docs/#select/suggest
                    */}
                    <StyledSearch
                        fluid
                        loading={this.state.isLoading && !!this.state.value}
                        onSearchChange={this.handleSearchChange}
                        resultRenderer={resultRenderer}
                        onResultSelect={this.handleResultSelect}
                        onFocus={this.handleOnFocus}
                        onBlur={this.handleOnBlur}
                        value={this.state.value}
                        results={this.state.results}
                        selectFirstResult
                    />
                </SearchQueryArea>

                <Hotkeys isSearching={this.state.focused} />
            </HeaderContainer>
        );
    }
}

export default SearchContainer;

import PropTypes from "prop-types";
import React from "react";
import { X } from "styled-icons/feather/X.cjs.js";

import { IconHolder, Title } from "./StyledEntry.jsx";

const UnknownEntry = ({ type }) => (
    <React.Fragment>
        <IconHolder size={56}>
            <X size={24} />
        </IconHolder>

        <Title>{type || <em>(missing type)</em>}</Title>
    </React.Fragment>
);

export default UnknownEntry;

UnknownEntry.propTypes = {
    type: PropTypes.string,
};

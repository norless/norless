import SongsRepository from "/imports/models/Songs.js";
import songParser from "/imports/utils/songParser.js";
import { withTracker } from "meteor/react-meteor-data";

import EntrySlides from "./EntrySlides.jsx";

const selectedEntry = new ReactiveVar(null);
export const setSelectedEntry = (entry) => selectedEntry.set(entry);

const EntrySlidesContainer = withTracker(() => {
    const entry = selectedEntry.get() || {};

    const data = {};
    switch (entry.type) {
        case undefined:
            // don't warn while loading
            break;
        case "song": {
            const song = SongsRepository.findOne({ _id: entry.identifier });
            data.title = song.title;
            data.slides = songParser({ lyrics: song.text });
            break;
        }
        case "divider":
            data.title = entry.identifier;
            break;
        default:
            console.error("Unknown EntrySlidesContainer type", entry);
            break;
    }

    // todo check edit permissions
    data.canEdit = true;

    return {
        ...data,

        entry,
    };
})(EntrySlides);

export default EntrySlidesContainer;

import React from "react";

import Routes from "./Routes.jsx";
import OutputWindowContainer from "./OutputWindowContainer.js";
import ConnectionStatus from "/imports/components/ConnectionStatus.jsx";

const App = () => (
    <>
        <ConnectionStatus />

        <Routes />

        <OutputWindowContainer />
    </>
);

export default App;

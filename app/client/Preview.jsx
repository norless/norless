import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ExternalLink } from "styled-icons/feather/ExternalLink.cjs.js";
import { Minimize2 } from "styled-icons/feather/Minimize2.cjs.js";

import OutputContainer from "./OutputContainer.js";
import outputWindowState from "./outputWindowState.js";

const OutputOpenerWrapper = styled.div`
    position: absolute;
    top: 5px;
    right: 5px;
    cursor: pointer;
    z-index: 1;
    color: white;
`;

const OutputOpener = props => {
    const Icon = props.outputWindowStateValue ? Minimize2 : ExternalLink;

    return (
        <OutputOpenerWrapper
            onClick={() => outputWindowState.set(!props.outputWindowStateValue)}
        >
            <Icon size={24} />
        </OutputOpenerWrapper>
    );
};

OutputOpener.propTypes = {
    outputWindowStateValue: PropTypes.bool,
};

const PreviewWindow = styled.div`
    position: relative;

    width: 240px;
    height: 180px;
`;

const Preview = props => (
    <>
        <PreviewWindow>
            <OutputOpener {...props} />
            <OutputContainer />
        </PreviewWindow>
    </>
);

Preview.propTypes = {
    slide_index: PropTypes.number,
    slide_count: PropTypes.number,
    key_signature: PropTypes.string,
    time_signature: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    next_line: PropTypes.string,
    form: PropTypes.string,
    is_last: PropTypes.bool,
};

export default Preview;

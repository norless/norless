import React from "react";
import { Meteor } from "meteor/meteor";
import { useParams } from "react-router-dom";
import { withTracker } from "meteor/react-meteor-data";

import Playlist from "./Playlist.jsx";
import PlaylistsRepository from "/imports/models/Playlists.js";

const PlaylistTracker = withTracker(({ playlistId }) => {
    const subscription = Meteor.subscribe("playlist", { playlistId });
    const loading = !subscription.ready();

    const playlist = playlistId
        ? PlaylistsRepository.findOne({ _id: playlistId })
        : PlaylistsRepository.findOne({}, { sort: { position: -1 } });

    return {
        loading,
        playlist,

        onSort: params => {
            Meteor.call("playlistOnSort", {
                _id: playlist._id,
                ...params,
            });
        },

        setSortedEntries: entries => {
            Meteor.call("playlistSetSortedEntries", {
                _id: playlist._id,
                entries,
            });
        },
    };
})(Playlist);

const PlaylistContainer = () => {
    const { playlistId } = useParams();

    return <PlaylistTracker playlistId={playlistId} />;
};

export default PlaylistContainer;

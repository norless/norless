import { withTracker } from "meteor/react-meteor-data";

import Preview from "./Preview.jsx";
import outputWindowState from "./outputWindowState.js";

const PreviewContainer = withTracker(() => ({
    outputWindowStateValue: outputWindowState.get(),
}))(Preview);

export default PreviewContainer;

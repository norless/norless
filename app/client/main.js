import "semantic-ui-css/semantic.min.css";
import "/imports/utils/reconnect.js";

import { Meteor } from "meteor/meteor";
import React from "react";
import { render } from "react-dom";

import App from "./App.jsx";

Meteor.startup(() => {
    Meteor.subscribe("bible_books");

    render(<App />, document.getElementById("app"));
});

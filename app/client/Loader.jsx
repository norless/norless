import React from "react";
import styled from "styled-components";
import { Loader as SemanticLoader } from "semantic-ui-react";

const Padder = styled.div`
    padding-top: 16px;
`;

const Loader = () => (
    <Padder>
        <SemanticLoader active inline="centered" />
    </Padder>
);

export default Loader;

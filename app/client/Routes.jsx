import { createBrowserHistory } from "history";
import React from "react";
import { Route, Router, Switch } from "react-router-dom";

import Layout from "./Layout.jsx";
import NotFound from "./NotFound.jsx";
import SongEditContainer from "./SongEditContainer.jsx";

const browserHistory = createBrowserHistory();

const Routes = () => (
    <Router history={browserHistory}>
        <Switch>
            <Route exact path="/" component={Layout} />
            <Route exact path="/playlist/:playlistId" component={Layout} />
            <Route exact path="/song/:songId" component={SongEditContainer} />
            <Route component={NotFound} />
        </Switch>
    </Router>
);
export default Routes;

import hasTimeouts from "/imports/hocs/hasTimeouts.js";
import split from "lodash/split";
import PropTypes from "prop-types";
import React from "react";
import { Textfit } from "react-textfit";
import styled, { css } from "styled-components";

import Loader from "./Loader.jsx";

const Wrapper = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;

    background: black;
    color: grey;

    font-family: Calibri, Arial, sans-serif;
    font-weight: bold;
    font-size: 8px;

    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    -webkit-tap-highlight-color: transparent;

    /* loader styles copied from */
    /* https://semantic-ui.com/dist/semantic.min.css */

    .ui.loader {
        display: none;
        position: absolute;
        top: 50%;
        left: 50%;
        margin: 0;
        text-align: center;
        z-index: 1000;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
    }
    .ui.loader:before {
        position: absolute;
        content: "";
        top: 0;
        left: 50%;
        width: 100%;
        height: 100%;
        border-radius: 500rem;
        border: 0.2em solid rgba(0, 0, 0, 0.1);
    }
    .ui.loader:after {
        position: absolute;
        content: "";
        top: 0;
        left: 50%;
        width: 100%;
        height: 100%;
        -webkit-animation: loader 0.6s linear;
        animation: loader 0.6s linear;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        border-radius: 500rem;
        border-color: #767676 transparent transparent;
        border-style: solid;
        border-width: 0.2em;
        -webkit-box-shadow: 0 0 0 1px transparent;
        box-shadow: 0 0 0 1px transparent;
    }
    @-webkit-keyframes loader {
        from {
            -webkit-transform: rotate(0);
            transform: rotate(0);
        }
        to {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes loader {
        from {
            -webkit-transform: rotate(0);
            transform: rotate(0);
        }
        to {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    .ui.mini.loader:after,
    .ui.mini.loader:before {
        width: 1rem;
        height: 1rem;
        margin: 0 0 0 -0.5rem;
    }
    .ui.tiny.loader:after,
    .ui.tiny.loader:before {
        width: 1.14285714rem;
        height: 1.14285714rem;
        margin: 0 0 0 -0.57142857rem;
    }
    .ui.small.loader:after,
    .ui.small.loader:before {
        width: 1.71428571rem;
        height: 1.71428571rem;
        margin: 0 0 0 -0.85714286rem;
    }
    .ui.loader:after,
    .ui.loader:before {
        width: 2.28571429rem;
        height: 2.28571429rem;
        margin: 0 0 0 -1.14285714rem;
    }
    .ui.large.loader:after,
    .ui.large.loader:before {
        width: 3.42857143rem;
        height: 3.42857143rem;
        margin: 0 0 0 -1.71428571rem;
    }
    .ui.big.loader:after,
    .ui.big.loader:before {
        width: 3.71428571rem;
        height: 3.71428571rem;
        margin: 0 0 0 -1.85714286rem;
    }
    .ui.huge.loader:after,
    .ui.huge.loader:before {
        width: 4.14285714rem;
        height: 4.14285714rem;
        margin: 0 0 0 -2.07142857rem;
    }
    .ui.massive.loader:after,
    .ui.massive.loader:before {
        width: 4.57142857rem;
        height: 4.57142857rem;
        margin: 0 0 0 -2.28571429rem;
    }
    .ui.dimmer .loader {
        display: block;
    }
    .ui.dimmer .ui.loader {
        color: rgba(255, 255, 255, 0.9);
    }
    .ui.dimmer .ui.loader:before {
        border-color: rgba(255, 255, 255, 0.15);
    }
    .ui.dimmer .ui.loader:after {
        border-color: #fff transparent transparent;
    }
    .ui.inverted.dimmer .ui.loader {
        color: rgba(0, 0, 0, 0.87);
    }
    .ui.inverted.dimmer .ui.loader:before {
        border-color: rgba(0, 0, 0, 0.1);
    }
    .ui.inverted.dimmer .ui.loader:after {
        border-color: #767676 transparent transparent;
    }
    .ui.text.loader {
        width: auto !important;
        height: auto !important;
        text-align: center;
        font-style: normal;
    }
    .ui.indeterminate.loader:after {
        animation-direction: reverse;
        -webkit-animation-duration: 1.2s;
        animation-duration: 1.2s;
    }
    .ui.loader.active,
    .ui.loader.visible {
        display: block;
    }
    .ui.loader.disabled,
    .ui.loader.hidden {
        display: none;
    }
    .ui.inverted.dimmer .ui.mini.loader,
    .ui.mini.loader {
        width: 1rem;
        height: 1rem;
        font-size: 0.78571429em;
    }
    .ui.inverted.dimmer .ui.tiny.loader,
    .ui.tiny.loader {
        width: 1.14285714rem;
        height: 1.14285714rem;
        font-size: 0.85714286em;
    }
    .ui.inverted.dimmer .ui.small.loader,
    .ui.small.loader {
        width: 1.71428571rem;
        height: 1.71428571rem;
        font-size: 0.92857143em;
    }
    .ui.inverted.dimmer .ui.loader,
    .ui.loader {
        width: 2.28571429rem;
        height: 2.28571429rem;
        font-size: 1em;
    }
    .ui.inverted.dimmer .ui.large.loader,
    .ui.large.loader {
        width: 3.42857143rem;
        height: 3.42857143rem;
        font-size: 1.14285714em;
    }
    .ui.big.loader,
    .ui.inverted.dimmer .ui.big.loader {
        width: 3.71428571rem;
        height: 3.71428571rem;
        font-size: 1.28571429em;
    }
    .ui.huge.loader,
    .ui.inverted.dimmer .ui.huge.loader {
        width: 4.14285714rem;
        height: 4.14285714rem;
        font-size: 1.42857143em;
    }
    .ui.inverted.dimmer .ui.massive.loader,
    .ui.massive.loader {
        width: 4.57142857rem;
        height: 4.57142857rem;
        font-size: 1.71428571em;
    }
    .ui.mini.text.loader {
        min-width: 1rem;
        padding-top: 1.78571429rem;
    }
    .ui.tiny.text.loader {
        min-width: 1.14285714rem;
        padding-top: 1.92857143rem;
    }
    .ui.small.text.loader {
        min-width: 1.71428571rem;
        padding-top: 2.5rem;
    }
    .ui.text.loader {
        min-width: 2.28571429rem;
        padding-top: 3.07142857rem;
    }
    .ui.large.text.loader {
        min-width: 3.42857143rem;
        padding-top: 4.21428571rem;
    }
    .ui.big.text.loader {
        min-width: 3.71428571rem;
        padding-top: 4.5rem;
    }
    .ui.huge.text.loader {
        min-width: 4.14285714rem;
        padding-top: 4.92857143rem;
    }
    .ui.massive.text.loader {
        min-width: 4.57142857rem;
        padding-top: 5.35714286rem;
    }
    .ui.inverted.loader {
        color: rgba(255, 255, 255, 0.9);
    }
    .ui.inverted.loader:before {
        border-color: rgba(255, 255, 255, 0.15);
    }
    .ui.inverted.loader:after {
        border-top-color: #fff;
    }
    .ui.inline.loader {
        position: relative;
        vertical-align: middle;
        margin: 0;
        left: 0;
        top: 0;
        -webkit-transform: none;
        transform: none;
    }
    .ui.inline.loader.active,
    .ui.inline.loader.visible {
        display: inline-block;
    }
    .ui.centered.inline.loader.active,
    .ui.centered.inline.loader.visible {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
`;

const Header = styled.div`
    position: absolute;
    left: 0%;
    right: 0%;
    top: 0%;
    height: 8%;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const styleContent = {
    position: "absolute",
    left: "0%",
    right: "0%",
    top: "0%",
    bottom: "0%",
};

const Content = styled.div`
    position: absolute;
    left: 0%;
    right: 0%;
    top: 8%;
    bottom: 8%;
    color: white;

    // so that textfit gets the width correctly
    display: inline-block;
    white-space: nowrap;

    &.R {
        font-style: italic;
    }

    .chord {
        position: absolute;
        z-index: 49;
        margin-top: -0.8em;
        font-size: 0.7em;
        color: grey;
    }

    .charpad {
        visibility: hidden;
    }

    ${({ isLast }) =>
        isLast &&
        css`
            p:last-child {
                &::after {
                    opacity: 0.5;
                    content: "*";
                }
            }
        `}
`;

const Footer = styled.div`
    position: absolute;
    left: 0%;
    right: 0%;
    bottom: 0%;
    height: 8%;
`;

class Output extends React.Component {
    state = {
        loading: true,
    };

    componentDidMount() {
        this.props.addTimeout(() => this.setState({ loading: false }), 500);
    }

    componentWillUnmount() {
        this.props.clearTimeouts();
    }

    render() {
        if (this.state.loading) {
            return (
                <Wrapper>
                    <Loader />
                </Wrapper>
            );
        }

        const props = this.props;

        console.log("Mounting the output");

        return (
            <Wrapper>
                <Header>
                    <span id="holder_slide_progress">
                        {props.slide_count
                            ? `#${1 + props.slide_index}/${props.slide_count}`
                            : ""}
                    </span>
                    &nbsp;
                    <span id="holder_key_signature">{props.key_signature}</span>
                    &nbsp;
                    <span id="holder_time_signature">
                        {props.time_signature}
                    </span>
                    &nbsp;
                    <span id="holder_title">{props.title}</span>
                </Header>
                <Content className={props.form} isLast={props.is_last}>
                    <Textfit mode="multi" style={styleContent}>
                        {split(props.text, /\n/g).map((line, index) => (
                            <p key={line + index}>
                                {line ? (
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: line,
                                        }}
                                    />
                                ) : (
                                    <span>&nbsp;</span>
                                )}
                            </p>
                        ))}
                    </Textfit>
                </Content>
                <Footer>{props.next_line}</Footer>
            </Wrapper>
        );
    }
}

Output.propTypes = {
    slide_index: PropTypes.number,
    slide_count: PropTypes.number,
    key_signature: PropTypes.string,
    time_signature: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    next_line: PropTypes.string,
    form: PropTypes.string,
    is_last: PropTypes.bool,
};

export default hasTimeouts(Output);

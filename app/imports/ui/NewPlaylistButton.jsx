import React from "react";
import moment from "moment";
import { useHistory } from "react-router-dom";
import { Plus } from "styled-icons/feather/Plus.cjs.js";

import Button from "/imports/components/Button.jsx";

const createPlaylist = ({ history }) => {
    const title = prompt("New playlist title", moment().format("D MMMM YYYY"));

    if (!title) {
        return;
    }

    Meteor.call(
        "playlistNew",
        {
            title,
        },
        (error, results) => {
            if (error) {
                console.error("Cannot get search results", error);
                return;
            }
            history.push("/");
        },
    );
};

const NewPlaylistButton = () => {
    const history = useHistory();

    return (
        <Button onClick={() => createPlaylist({ history })}>
            <Plus size="24" />
            New playlist
        </Button>
    );
};

export default NewPlaylistButton;

import { Blaze } from "meteor/blaze";
import { Template } from "meteor/templating";
import React, { useEffect, useRef } from "react";
import styled from "styled-components";

const Elements = styled.div`
    .accounts-dialog {
        position: absolute;
        top: 0px;
        background: white;
        z-index: 999;
        padding: 10px;
        border: 2px solid #237cd7;
        border-radius: 3px;
        box-shadow: 2px 2px 1vmax 0px rgba(0, 0, 0, 0.4);
    }

    .loading {
        position: absolute;
        top: 10px;
        right: 10px;
        &::after {
            content: "☁️";
            color: #237cd7;
        }
    }

    .login-form {
        label {
            display: block;
            color: #237cd7;
        }

        input {
            border: 2px solid #237cd7;
            padding: 3px;
            border-radius: 3px;
            margin-bottom: 0.5em;
        }
    }

    .error-message {
        color: #d74d23;
        max-width: 156px;
        font-size: 0.7em;
        line-height: 1.2em;
        margin-bottom: 10px;
    }

    .login-button {
        background: #237cd7;
        color: white;

        font-size: 1em;
        padding: 0.25em 1em;
        border: 2px solid #237cd7;
        border-radius: 3px;
        text-align: center;
        margin-bottom: 0.5em;
        width: 100%;
    }

    .additional-link,
    .login-close-text {
        color: #237cd7;
        text-decoration: underline;
    }

    .login-close-text-clear {
        margin-bottom: 10px;
    }
`;

const AccountsUIWrapper = () => {
    const container = useRef(null);

    useEffect(() => {
        const view = Blaze.render(Template.loginButtons, container.current);
        return () => Blaze.remove(view);
    });

    return (
        <Elements>
            <div ref={container} />
        </Elements>
    );
};

export default AccountsUIWrapper;

import get from "lodash/get";
import { withTracker } from "meteor/react-meteor-data";
import PropTypes from "prop-types";
import React from "react";
import Avatar from "react-avatar";

const withMyUser = withTracker(() => ({
    user: Meteor.user(),
}));

const withIdToUser = withTracker(({ id }) => ({
    user: Meteor.users.findOne(id),
}));

export const getName = (user) =>
    get(user, "profile.name") ||
    get(user, "username") ||
    get(user, "_id") ||
    "(no user)";

const Icon = ({ user }) =>
    user ? (
        <Avatar
            src={get(user, "services.google.picture")}
            size={12}
            round={true}
            name={getName(user)}
        />
    ) : null;

Icon.propTypes = {
    user: PropTypes.object,
};

export const MyIcon = withMyUser(Icon);

export const IconName = ({ user }) => (
    <>
        <Icon user={user} />
        &nbsp;
        {getName(user)}
    </>
);

IconName.propTypes = {
    user: PropTypes.object,
};

export const IdToIconName = withIdToUser(IconName);

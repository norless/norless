import React from "react";
import { useHistory } from "react-router-dom";
import { Settings } from "styled-icons/feather/Settings.cjs.js";

import Button from "/imports/components/Button.jsx";

export const SettingsButton = () => {
    const history = useHistory();

    return (
        <Button onClick={() => history.push("/settings")}>
            <Settings size="24" />
            Settings
        </Button>
    );
};

import min from "lodash/min";
import max from "lodash/max";
import escapeminus from "/imports/utils/escapeminus.js";
import removeDiacritics from "/imports/utils/removeDiacritics.js";
import correctTypography from "/imports/utils/correctTypography.js";

const Bible = new Meteor.Collection("bible");
const BibleBooks = new Meteor.Collection("bible_books");

const re = /^\s*([0-9]?[^0-9]+)([^a-z0-9]*([0-9]+))([^0-9]+([0-9]+)([^0-9]+([0-9]+))?)?\s*$/;

const getTitle = (bookNumber, chapterNumber, verse1, verse2) => {
    try {
        const title = correctTypography(
            BibleBooks.findOne({ _id: bookNumber }).title,
        );

        return `${title} ${chapterNumber}:${verse1}${
            typeof verse2 !== "undefined" && verse1 !== verse2
                ? `-${verse2}`
                : ""
        }`;
    } catch (e) {
        console.error("Error getting title", e);
        return `Error in book ${bookNumber} ${chapterNumber} ${verse1} ${verse2}`;
    }
};

const convertIdToTitle = (id, index) => {
    const match = `${id}`.match(/^b([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)$/);

    if (typeof index !== "undefined") {
        return getTitle(match[1], match[2], index + 1, index + 1);
    }

    return getTitle(match[1], match[2], match[3], match[4]);
};

const searchBooks = query => {
    const match = removeDiacritics(query).match(re);

    if (!match) {
        return [];
    }

    const book = escapeminus(match[1]);
    let chapter = 1;
    let verse1 = 1;
    let verse2 = 1;

    if (parseInt(match[3], 10)) {
        chapter = parseInt(match[3], 10);
    }

    if (parseInt(match[5], 10)) {
        verse1 = parseInt(match[5], 10);
        verse2 = verse1;
    }

    if (parseInt(match[7], 10)) {
        verse2 = parseInt(match[7], 10);
    }

    if (verse2 < verse1) {
        [verse1, verse2] = [verse2, verse1];
    }

    return BibleBooks.find({
        title_escaped: new RegExp(book),
    }).map(b => {
        const chapterCapped = max([1, min([chapter, b.chapters])]);
        const verse1Capped = max([
            1,
            min([verse1, b.chapter_lengths[chapterCapped]]),
        ]);
        const verse2Capped = max([
            1,
            min([verse2, b.chapter_lengths[chapterCapped]]),
        ]);

        const id = `b${b._id}_${chapterCapped}_${verse1Capped}_${verse2Capped}`;

        return {
            title: convertIdToTitle(id),
            klass: "bible",
            _id: id,
        };
    });
};

export { Bible, BibleBooks, searchBooks };

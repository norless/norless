import ListOfPlaylistsContainer from "/client/ListOfPlaylistsContainer.jsx";
import ListOfUsersContainer from "/client/ListOfUsersContainer.jsx";
import InstallButton from "/imports/pwa/InstallButton.jsx";
import AccountsUIWrapper from "/imports/ui/AccountsUIWrapper.jsx";
import NewPlaylistButton from "/imports/ui/NewPlaylistButton.jsx";
import { SettingsButton } from "/imports/ui/Settings.jsx";
import { MyIcon } from "/imports/ui/User.jsx";
import React, { useCallback, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import styled, { css } from "styled-components";
import { Menu } from "styled-icons/feather/Menu.cjs.js";

const OpenerContainer = styled.div`
    position: ${(props) => (props.opened ? "" : "absolute")};
    color: ${(props) => (props.opened ? "#237cd7" : "white")};
    width: 56px;
    line-height: 56px;
    text-align: center;
    cursor: pointer;
`;

export const BurgerMenuOpener = (props) => (
    <OpenerContainer {...props} onClick={() => window.toggleBurgerMenu()}>
        <Menu size={32} />
    </OpenerContainer>
);

const Veil = styled.div`
    z-index: 5550;
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    background: #000000;
    cursor: pointer;
    opacity: 0.4;
    margin-left: 0%;
    transition: opacity 0.3s ease-out;

    ${(props) =>
        props.closed &&
        css`
            opacity: 0;
            margin-left: 100%;
            transition: margin-left 0s 0.3s, opacity 0.3s ease-out;
        `}
`;

const ContentContainer = styled.div`
    z-index: 5600;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    background: white;
    right: 56px;
    max-width: 500px;
    box-shadow: 0px 0px 32px rgba(0, 0, 0, 0.37);
    transition: all 0.7s;
    transition-timing-function: cubic-bezier(0.07, 0.895, 0.11, 1);
    /* http://matthewlein.com/ceaser/ custom */

    ${(props) =>
        props.closed &&
        css`
            right: 100%;
            left: -100%;
            box-shadow: none;
        `}
`;

const AccountsContainer = styled.div`
    position: relative;
    margin-left: 15px;
    padding-top: 10px;
    display: flex;
`;

const Header = styled.div`
    display: flex;
`;

const HeaderTitle = styled.h1`
    height: 56px;
    line-height: 56px;
    margin: 0;
    padding-left: 15px;
`;

const Logo = styled.div`
    height: 56px;
    width: 56px;
    background-image: url(/images/icon-128x128.png);
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
`;

const Spacer = styled.div`
    margin-left: auto;
`;

const Content = () => (
    <>
        <Header>
            <BurgerMenuOpener opened />
            <Logo />
            <Spacer />
            <InstallButton />
            <SettingsButton />
        </Header>

        <AccountsContainer>
            <MyIcon />
            <AccountsUIWrapper />
        </AccountsContainer>

        <Header>
            <HeaderTitle>Playlists</HeaderTitle>
            <Spacer />
            <NewPlaylistButton />
        </Header>
        <ListOfPlaylistsContainer />

        <Header>
            <HeaderTitle>Users</HeaderTitle>
        </Header>
        <ListOfUsersContainer />
    </>
);

export const BurgerMenuContent = () => {
    const [closed, setClosed] = useState(true);

    useHotkeys("esc", () => setClosed(true));

    window.toggleBurgerMenu = useCallback((close) =>
        setClosed(typeof close === "undefined" ? !closed : !close),
    );

    return (
        <>
            <Veil closed={closed} onClick={() => setClosed(true)} />
            <ContentContainer closed={closed}>
                <Content />
            </ContentContainer>
        </>
    );
};

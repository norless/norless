// styled components portal inspired from
// https://github.com/Briggybros/styled-window-portal

// self closing window inspired from
// https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202

import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { StyleSheetManager } from "styled-components";

class StyledWindowPortal extends React.PureComponent {
    static defaultProps = {
        onClose: () => {},
        title: "New Window",
        windowProps: {
            toolbar: false,
            location: false,
            directories: false,
            status: false,
            menubar: false,
            scrollbars: false,
            resizable: true,
            width: 500,
            height: 400,
            top: (props, window) =>
                window.screen.height / 2 - window.outerHeight / 2,
            left: (props, window) =>
                window.screen.width / 2 - window.outerWidth / 2,
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            externalWindow: null,
        };
        this.container = document.createElement("div");
    }

    componentDidMount() {
        this.setState(
            {
                externalWindow: window.open("", "", this.windowPropsToString()),
            },
            () => {
                if (this.state.externalWindow != null) {
                    this.state.externalWindow.addEventListener(
                        "unload",
                        this.props.onClose,
                    );

                    const title = this.state.externalWindow.document.createElement(
                        "title",
                    );
                    title.innerText = this.props.title || "";
                    this.state.externalWindow.document.head.appendChild(title);

                    this.state.externalWindow.document.body.appendChild(
                        this.container,
                    );

                    // Inject global style
                    Array.from(document.head.getElementsByTagName("STYLE"))
                        .filter(
                            style =>
                                style.innerText.indexOf(
                                    "\n/* sc-component-id: sc-global",
                                ) !== -1,
                        )
                        .forEach(style => {
                            if (this.state.externalWindow != null) {
                                this.state.externalWindow.document.head.appendChild(
                                    style.cloneNode(true),
                                );
                            }
                        });
                }
            },
        );

        window.addEventListener("unload", this.closeExternalWindow);
    }

    componentWillUnmount() {
        window.removeEventListener("unload", this.closeExternalWindow);

        this.closeExternalWindow();
    }

    closeExternalWindow = () => {
        if (this.state.externalWindow && !this.state.externalWindow.closed) {
            this.state.externalWindow.close();
        }
    };

    windowPropsToString() {
        const mergedProps = {
            ...StyledWindowPortal.defaultProps.windowProps,
        };

        return Object.keys(mergedProps)
            .map(key => {
                switch (typeof mergedProps[key]) {
                    case "function":
                        return `${key}=${mergedProps[key].call(
                            this,
                            mergedProps,
                            window,
                        )}`;
                    case "boolean":
                        return `${key}=${mergedProps[key] ? "yes" : "no"}`;
                    default:
                        return `${key}=${mergedProps[key]}`;
                }
            })
            .join(",");
    }

    render() {
        if (!this.state.externalWindow) {
            return null;
        }

        return (
            <StyleSheetManager target={this.state.externalWindow.document.head}>
                <div>
                    {ReactDOM.createPortal(this.props.children, this.container)}
                </div>
            </StyleSheetManager>
        );
    }
}

StyledWindowPortal.propTypes = {
    onClose: PropTypes.func,
    title: PropTypes.string,
    children: PropTypes.node.isRequired,
};

export { StyledWindowPortal };

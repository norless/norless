import styled from "styled-components";

const Button = styled.button`
    background: white;
    color: #237cd7;

    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid #237cd7;
    border-radius: 3px;
    text-align: center;

    svg {
        margin-right: 5px;
    }
`;

export default Button;

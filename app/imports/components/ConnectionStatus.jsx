import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { withTracker } from "meteor/react-meteor-data";
import { Cloud } from "styled-icons/feather/Cloud.cjs.js";
import { Loader } from "styled-icons/feather/Loader.cjs.js";
import { XCircle } from "styled-icons/feather/XCircle.cjs.js";
import { CloudOff } from "styled-icons/feather/CloudOff.cjs.js";

const Container = styled.div`
    z-index: 7000;
    position: absolute;
    right: 16px;
    top: 16px;
    background: #237cd7;
    color: #ffffff;
    font-size: 24px;
    text-align: center;
    width: 56px;
    height: 56px;
    border-radius: 28px;
    box-shadow: 0 6px 6px rgba(0, 0, 0, 0.32);
    transition: all 0.7s;
    transition-timing-function: cubic-bezier(0.07, 0.895, 0.11, 1);
    /* http://matthewlein.com/ceaser/ custom */

    ${({ closed }) =>
        closed &&
        css`
            transition: top 0.7s 2s;
            top: -80px;
        `}
`;

const Spin = styled.div`
    margin-top: 15px;

    ${({ spin }) =>
        spin &&
        css`
            @keyframes rotating {
                from {
                    transform: rotate(0deg);
                }
                to {
                    transform: rotate(360deg);
                }
            }
            animation: rotating 2s linear infinite;
        `}
`;

const icons = {
    connected: <Cloud size="24" />,
    connecting: <Loader size="24" />,
    waiting: <CloudOff size="24" />,
    offline: <CloudOff size="24" />,
    failed: <XCircle size="24" />,
};

const ConnectionStatus = ({ status }) => (
    <Container closed={status === "connected"}>
        <Spin spin={status === "connecting"}>
            {icons[status] || icons.failed}
        </Spin>
    </Container>
);

ConnectionStatus.propTypes = {
    status: PropTypes.string,
};

export default withTracker(() => ({
    status: Meteor.status().status,
}))(ConnectionStatus);

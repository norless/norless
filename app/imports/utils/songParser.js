import map from "lodash/map";
import each from "lodash/each";
import cloneDeep from "lodash/cloneDeep";
import correctTypography from "/imports/utils/correctTypography.js";

const parseChords = line => {
    // add space at the end
    const chordLine = `${line} `;

    const chords = [];
    let chord = "";
    let pos = 0;
    for (let i = 0; i < chordLine.length; i++) {
        if (chordLine[i] !== " ") {
            chord += chordLine[i];
        } else {
            if (chord !== "") {
                chords.push({
                    chord,
                    pos,
                });
            }

            pos = i + 1;
            chord = "";
        }
    }

    return chords;
};

// public
const songParser = ({ lyrics = "" }) => {
    let verses = [];

    const versesNamed = {};

    const lines = lyrics.split(/\n/);

    if (lines[lines.length - 1] !== "") {
        // add an empty line at the end, for automated parsing
        lines.push("");
    }

    let verseBuilder = "";
    let verseForm = "";
    let chords = [];

    const parseLine = originalLine => {
        let line = originalLine;

        const matchVerseFormColon = line.match(/^([a-z0-9+]):$/i);
        if (matchVerseFormColon) {
            // console.info('Setting verse form: ' + line)
            verseForm = matchVerseFormColon[1];
            return;
        }

        const matchVerseFormBrackets = line.match(/^\[([a-z0-9]+)\]$/i);

        if (matchVerseFormBrackets) {
            // console.info('Setting verse form: ' + line)
            verseForm = matchVerseFormBrackets[1];
            return;
        }

        const matchChords = line.match(/^\.(.+)$/);

        if (matchChords) {
            // console.log('Setting next chords:  ' + line)
            chords = parseChords(matchChords[1]);
            return;
        }

        if (line === ".") {
            verseBuilder += "\n";
            return;
        }

        // console.log('Adding line to verse: "' + line + '"')

        if (chords.length) {
            // add trailing characters when chord line is longer than verse line
            while (line.length < chords[chords.length - 1].pos) {
                // add invisible character
                line += "~";
            }
        }

        const originalLineLength = line.length;

        for (
            let chordReverseIndex = chords.length - 1;
            chordReverseIndex >= 0;
            chordReverseIndex--
        ) {
            // process in reverse order, so we can use character positions
            let chordpos = chords[chordReverseIndex].pos;

            // count the spaces at line start
            chordpos = chordpos + 1 - line.search(/\S/);

            if (chordpos > originalLineLength) {
                line = `${line}<span class="chord">${
                    chords[chordReverseIndex].chord
                }</span>`;
            } else if (chordpos < 0) {
                // don't add that chord
            } else {
                line = `${line.substr(0, chordpos)}</span><span class="chord">${
                    chords[chordReverseIndex].chord
                }</span><span class="text">${line.substr(chordpos)}`;
            }
        }

        if (chords.length) {
            // use simple text when there are no chords
            // so it can be used as a pointer destination, like "R"
            line = `<span class="text">${line}</span>`;
        }

        chords = [];

        // make the character invisible
        line = line.replace(/(~+)/g, '<span class="charpad">$1</span>');

        // strip unnecessary spaces
        line = line.replace(/_+/g, "").replace(/\s+/, " ");

        // console.log('Adding line to verse: "' + line + '"')
        verseBuilder += `${line}\n`;
    };

    each(lines, originalLine => {
        const line = originalLine.trim();

        if (line !== "") {
            parseLine(line);
            return;
        }

        // empty line
        verseBuilder = verseBuilder.trim();
        if (verseBuilder !== "") {
            const verse = {};

            if (verseForm !== "") {
                verse.form = verseForm;
            }

            verseBuilder = correctTypography(verseBuilder);

            verse.text = verseBuilder;

            // console.info('Adding verse', verse)
            verses.push(verse);

            if (verseForm !== "") {
                versesNamed[verseForm] = verse;
                verseForm = "";
            }
        }

        verseBuilder = "";
    });

    verses = map(verses, (verse, index) => {
        const expanded = versesNamed[verse.text]
            ? cloneDeep(versesNamed[verse.text])
            : verse;

        if (index === verses.length - 1) {
            expanded.is_last = true;
        }

        return expanded;
    });

    return verses;
};

export default songParser;

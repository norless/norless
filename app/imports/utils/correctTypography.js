const correctTypography = str =>
    str
        .replace(/-/g, "‑") // non breaking hyphen (&#8209;)
        .replace(/ş/g, "ș")
        .replace(/Ş/g, "Ș")
        .replace(/ţ/g, "ț")
        .replace(/Ţ/g, "Ț");

export default correctTypography;

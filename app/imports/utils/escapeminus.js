import removeDiacritics from "/imports/utils/removeDiacritics.js";

const escapeminus = (str, separator = "-") =>
    removeDiacritics(str)
        .toLowerCase()
        .replace(/[^a-z0-9]+/g, separator)
        .replace(/^-|-$/g, "");

export default escapeminus;

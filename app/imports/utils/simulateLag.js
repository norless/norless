/* eslint no-underscore-dangle: 0 */

const simulateLag = () => {
    if (Meteor.isDevelopment) {
        Meteor._sleepForMs(800);
    }
};

export default simulateLag;

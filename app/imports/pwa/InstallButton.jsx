import React, { Component } from "react";
import { DownloadCloud } from "styled-icons/feather/DownloadCloud.cjs.js";

import Button from "/imports/components/Button.jsx";

export default class InstallButton extends Component {
    state = {
        prompt: null,
    };

    install = async () => {
        if (!this.state.prompt) return;

        this.state.prompt.prompt();

        await this.state.prompt.userChoice;

        this.setState({
            prompt: null,
        });
    };

    componentDidMount() {
        window.addEventListener("beforeinstallprompt", e => {
            e.preventDefault();

            this.setState({
                prompt: e,
            });
        });
    }

    render() {
        if (!this.state.prompt) {
            return null;
        }

        return (
            <Button onClick={this.install}>
                <DownloadCloud size={24} />
                Install
            </Button>
        );
    }
}

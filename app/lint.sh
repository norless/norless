#!/bin/bash

set -ueo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

echo "Linting source code"
./node_modules/.bin/eslint \
--ext .js --ext .jsx \
. $*

echo "Linting the linter config"
./node_modules/.bin/eslint \
--no-ignore \
.eslintrc.js \
$*

echo "Done"

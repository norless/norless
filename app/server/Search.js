import SongsRepository from "/imports/models/Songs.js";
import removeDiacritics from "/imports/utils/removeDiacritics.js";
import simulateLag from "/imports/utils/simulateLag.js";
import forEach from "lodash/forEach";
import keyBy from "lodash/keyBy";
import map from "lodash/map";
import lunr from "lunr";

let index = null;

Meteor.startup(() => {
    console.log("Creating songs search index");

    index = lunr(function initiateIndex() {
        this.field("title", { boost: 10 });
        this.field("text");
        this.ref("_id");

        const addSong = (song) => {
            if (!song.title) {
                return;
            }

            // console.log("Adding song to search index", song._id, song.title);

            this.add({
                ...song,
                title: removeDiacritics(song.title),
                text: removeDiacritics(song.text),
            });
        };

        const removeSong = (song) => {
            console.log(
                "Removing song from search index",
                song._id,
                song.title,
            );
            this.remove(song);
        };

        SongsRepository.find().observe({
            added: addSong,
            changed: addSong,
            removed: removeSong,
        });
    });
});

// retrive results
Meteor.methods({
    getSearchResults({ query }) {
        simulateLag();

        if (!index) {
            throw new Error("Search index not yet initialized");
        }

        if (!query) {
            console.log("Searching without query");
            return [];
        }

        console.log("Searching for", query);

        check(query, String);

        const ret = [];

        // add songs
        let results = index.search(removeDiacritics(query));

        results = results.slice(0, 25);

        const songs = keyBy(
            SongsRepository.find(
                {
                    _id: { $in: map(results, (result) => result.ref) },
                },
                {
                    fields: {
                        title: 1,
                        key_signature: 1,
                    },
                },
            ).fetch(),
            "_id",
        );

        forEach(results, (result) => {
            const song = songs[result.ref];

            if (!song) {
                console.error("Matched song not found in database", result);
                return;
            }

            ret.push({
                ...song,
                type: "song",
            });
        });

        return ret;
    },
});

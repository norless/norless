import { publishComposite } from "meteor/reywood:publish-composite";

import simulateLag from "/imports/utils/simulateLag.js";
import PlaylistsRepository from "/imports/models/Playlists.js";

publishComposite("list_of_playlists", () => {
    console.log("publish list of playlists");

    simulateLag();

    return {
        find() {
            return PlaylistsRepository.find(
                {
                    location_id: "unuunu",
                },
                {
                    fields: {
                        title: 1,
                        creator: 1,
                        position: 1,
                    },
                    sort: {
                        position: -1,
                    },
                    limit: 20,
                },
            );
        },
        children: [
            {
                find(playlist) {
                    return Meteor.users.find(
                        {
                            _id: playlist.creator,
                        },
                        {
                            fields: {
                                status: true,
                                "profile.name": true,
                                username: true,
                            },
                        },
                    );
                },
            },
        ],
    };
});

Meteor.methods({
    playlistNew: ({ title }) => {
        console.log("playlistNew", title);

        simulateLag();

        // todo check permission to create new playlist

        check(title, String);

        if (!title) {
            throw new Meteor.Error(400, "Title must not be empty");
        }

        if (
            PlaylistsRepository.find({
                location_id: "unuunu",
                title,
            }).count()
        ) {
            throw new Meteor.Error(
                400,
                `You already have a playlist named ${title}`,
            );
        }

        const playlistId = PlaylistsRepository.insert({
            location_id: "unuunu",
            title,
            created_at: new Date(),
            position: new Date().getTime(),
            creator: Meteor.userId(),
        });

        return { playlistId };
    },
});

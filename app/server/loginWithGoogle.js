Meteor.startup(() => {
    const clientId = process.env.GOOGLE_LOGIN_CLIENT_ID;

    if (!clientId) {
        console.error(
            "Please provide GOOGLE_LOGIN_CLIENT_ID environment variable",
        );
    }

    ServiceConfiguration.configurations.upsert(
        {
            service: "google",
        },
        {
            $set: {
                loginStyle: "redirect",
                clientId,
                secret: process.env.GOOGLE_LOGIN_SECRET,
            },
        },
    );
});

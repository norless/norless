import { BibleBooks } from "/imports/models/Bible.js";
import simulateLag from "/imports/utils/simulateLag.js";

Meteor.publish("bible_books", () => {
    console.log("publish bible_books");

    simulateLag();

    return BibleBooks.find();
});

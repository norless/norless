import simulateLag from "/imports/utils/simulateLag.js";
import { UserStatus } from "meteor/mizzao:user-status";

Meteor.publish("userStatus", () => {
    console.log("publish user status");

    simulateLag();

    return [
        Meteor.users.find(
            {
                // todo filter by location_id
            },
            {
                fields: {
                    status: true,
                    "profile.name": true,
                    username: true,
                    "services.google.picture": true,
                },
            },
        ),

        UserStatus.connections.find(
            {
                // todo filter by location_id
            },
            {
                fields: {
                    userId: true,
                    userAgent: true,
                },
            },
        ),
    ];
});

import { check } from "meteor/check";
import { publishComposite } from "meteor/reywood:publish-composite";
import map from "lodash/map";
import filter from "lodash/filter";

import PlaylistsRepository from "/imports/models/Playlists.js";
import SongsRepository from "/imports/models/Songs.js";
import simulateLag from "/imports/utils/simulateLag.js";

publishComposite("playlist", ({ playlistId }) => {
    console.log("publish playlist", { playlistId });

    simulateLag();

    return {
        find() {
            return PlaylistsRepository.find(
                {
                    location_id: "unuunu",
                    ...(playlistId ? { _id: playlistId } : {}),
                },
                {
                    sort: {
                        position: -1,
                    },
                    limit: 1,
                },
            );
        },
        children: [
            {
                find(playlist) {
                    const songIds = map(
                        filter(playlist.entries, { type: "song" }),
                        "identifier",
                    );

                    // publish songs in this playlist
                    return SongsRepository.find({
                        _id: { $in: songIds },
                    });
                },
            },
        ],
    };
});

Meteor.methods({
    playlistOnSort: ({ _id, entry, active, oldIndex, newIndex, sortHash }) => {
        console.log(
            "playlistOnSort",
            _id,
            entry,
            active,
            oldIndex,
            newIndex,
            sortHash,
        );

        simulateLag();

        check(_id, String);
        check(entry, { type: String, identifier: String });
        check(active, Boolean);
        check(oldIndex, Number);
        check(newIndex, Number);
        check(sortHash, String);

        PlaylistsRepository.update(
            {
                _id,
                location_id: "unuunu",
            },
            {
                $set: {
                    sorting: {
                        username: "todo",
                        type: entry.type,
                        identifier: entry.identifier,
                        active,
                        oldIndex,
                        newIndex,
                        sortHash,
                    },
                },
            },
            {
                multi: false,
                upsert: false,
            },
        );
    },
});

Meteor.methods({
    playlistSetSortedEntries: ({ _id, entries }) => {
        console.log("playlistSetSortedEntries", _id);

        simulateLag();

        check(_id, String);
        check(entries, [Object]);

        PlaylistsRepository.update(
            {
                _id,
                location_id: "unuunu",
            },
            {
                $set: {
                    entries,
                },
                $unset: {
                    sorting: true,
                },
            },
            {
                multi: false,
                upsert: false,
            },
        );
    },
});

import SongsRepository from "/imports/models/Songs.js";
import simulateLag from "/imports/utils/simulateLag.js";
import { each } from "lodash";

Meteor.publish("song", ({ songId }) => {
    console.log("publish song", { songId });

    simulateLag();

    return SongsRepository.find({
        _id: songId,
    });
});

Meteor.publish("song_tags", () => {
    console.log("publish song_tags");

    simulateLag();

    return SongsRepository.find(
        {
            tags: { $exists: true },
        },
        {
            fields: {
                tags: 1,
            },
        },
    );
});

const valuesToSet = (values) => {
    const set = {};

    each(["title", "key_signature", "time_signature", "text", "tags"], (k) => {
        if (values[k]) {
            set[k] = values[k];
        }
    });

    return set;
};

Meteor.methods({
    songInsert: ({ values }) => {
        console.log("method songInsert", values);

        simulateLag();

        const songId = SongsRepository.insert({
            ...valuesToSet(values),
            creator: Meteor.userId(),
        });

        return { songId };
    },
});

Meteor.methods({
    songUpdate: ({ _id, values }) => {
        console.log("method songUpdate", _id, values);

        simulateLag();

        SongsRepository.update({ _id }, { $set: valuesToSet(values) });
    },
});

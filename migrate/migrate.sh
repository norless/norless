#!/bin/bash

set -uexo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

ssh ubuntu@norless \
mongodump --archive --gzip \
--db norless \
--numParallelCollections=1 \
| docker exec -i mongo \
mongorestore --archive --gzip \
--noIndexRestore \
--drop \

cat unuunu.js \
| docker exec -i mongo mongo norless

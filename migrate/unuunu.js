print("Embedding entries into playlists");

db.playlists.find({
    location_id: { $exists: false },
}).forEach(p => {
    const entries = db.entries.find({
        playlist_id: p._id,
    }).sort({
        position: 1,
    }).map(e => {
        e.type = e.klass;

        delete e._id;
        delete e.playlist_id;
        delete e.position;
        delete e.nocache;
        delete e.klass;

        return e;
    });

    db.playlists.update(
        {
            _id: p._id,
        },
        {
            $set: {
                location_id: "unuunu",
                entries,
            },
        }
    );
});

print("Removing star from end of songs");

db.songs.find({ text: /\*\s*$/ }).forEach(s => {
    const text = s.text.replace(/\*(\s*)$/m, "$1");

    db.songs.update({ _id: s._id }, { $set: { text } });
});

print("Done");

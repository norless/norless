# Norless

## Install

- ```sh install meteor npm install -g meteor```
- install mongo (community edition)
- ```mongorestore --archive=~/Downloads/norless.mongodump.gz --gzip```

## Usage

start the app with:

```sh
app/run-local.sh
```

